require('dotenv').config()

var express = require('express');
var logger = require('morgan');
var bodyParser = require('body-parser');

var app = express();

app.use(logger('dev'));
app.use(bodyParser.json({limit: '50mb'})); // since i am transferring base64 image inside json
app.use(bodyParser.urlencoded({limit: '50mb', extended: true}));

app.all('/*', function (req, res, next) {
    // CORS headers
    res.header("Access-Control-Allow-Origin", "*"); // restrict it to the required domain
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS');
    // Set custom headers for CORS
    res.header('Access-Control-Allow-Headers', 'Content-type,Accept,If-Modified-Since,X-Access-Token,Cache-Control');
    if (req.method === 'OPTIONS') {
        res.status(200).end();
    } else {
        next();
    }
});

app.use('/', require('./src'));

// If no route is matched by now, it must be a 404
app.use(function (req, res) {
    res.status(404);
    res.json({
        status: 404,
        message: "Page Not Found"
    });
});

// Start the server

var Config = require('./src/config/config');

var port = Config.server.port;
var server = app.listen(port, function () {
    console.log('Express server listening on port ' + port);
});

