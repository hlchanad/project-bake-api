
var express = require('express');
var router = express.Router();

var formValidation = require('./form-validation.js');

router.post('/user/create', formValidation.create);
router.post('/user/login', formValidation.userLogin);

router.post('/device/regToken', formValidation.deviceRegToken);

router.get('/recipe/:id', formValidation.getRecipeById);
router.post('/recipe', formValidation.addRecipe);
router.post('/recipe/full-json', formValidation.addRecipeJSON);
router.put('/recipe/:id', formValidation.editRecipe);
router.put('/recipe/:id/activate', formValidation.activateRecipe);
router.put('/recipe/:id/suspend', formValidation.suspendRecipe);
router.delete('/recipe/:id', formValidation.deleteRecipe);

router.get('/stat/count/:type', formValidation.statGetCount);

module.exports = router;