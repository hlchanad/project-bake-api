
var Promise = require('promise');

var storage = require('../shared/storage/storage.js')();
var ModelHelper = require('../shared/model-helper.js');
var Response = require('../shared/response.js');

module.exports = {
    addRecipeUploadImages: function(req, res, next) {

        var promises = [];

        promises.push(storage.storeBase64Image(req.body.thumbnail)
            .then(function(link) {
                res.locals.recipe.thumbnail = { imageUrl: link };
            })
            .catch(function(error) {
                return Promise.reject(error);
            })
        );

        res.locals.recipe.images = [];
        req.body.images.forEach(function(image) {
            promises.push(storage.storeBase64Image(image)
                .then(function(link) {
                    res.locals.recipe.images.push({ imageUrl: link });
                })
                .catch(function(error) {
                    return Promise.reject(error);
                })
            );
        });

        Promise.all(promises)
            .then(function() {
                next();
            })
            .catch(function(error) {
                res.status(error.status)
                    .json(Response.api(error.status, error.message));
            });
    },

    editRecipeUploadImages: function(req, res, next) {

        var promises = [];

        if (!ModelHelper.isValidBase64Format(req.body.thumbnail)) {
            // thumbnail is simply a link
            res.locals.recipe.thumbnail = { imageUrl: req.body.thumbnail };
        }
        else {
            // thumbnail is a base64 encoded image -> store it
            promises.push(storage.storeBase64Image(req.body.thumbnail)
                .then(function(link) {
                    res.locals.recipe.thumbnail = { imageUrl: link };
                })
                .catch(function(error) {
                    return Promise.reject(error);
                })
            );
        }

        res.locals.recipe.images = new Array(req.body.images.length);
        for (var i = 0; i < req.body.images.length; i++) {
            if (!ModelHelper.isValidBase64Format(req.body.images[i])) {
                // image is simply a link
                res.locals.recipe.images[i] = { imageUrl: req.body.images[i] };
            }
            else {
                // image is a base64 encoded image -> store it
                promises.push(storage.storeBase64Image(req.body.images[i], null, null, null, { index: i })
                    .then(function(data) {
                        res.locals.recipe.images[data.payload.index] = { imageUrl: data.link };
                    })
                    .catch(function(error) {
                        return Promise.reject(error);
                    })
                );
            }
        }

        Promise.all(promises)
            .then(function() {
                next();
            })
            .catch(function(error) {
                res.status(error.status).json(error.status, error.message);
            })
    }
}