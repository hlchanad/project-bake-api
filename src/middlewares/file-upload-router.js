
var express = require('express');
var router = express.Router();

var fileUpload = require('./file-upload.js');

router.post('/recipe', fileUpload.addRecipeUploadImages);
router.put('/recipe/:id', fileUpload.editRecipeUploadImages);

module.exports = router;