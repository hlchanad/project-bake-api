
var HttpStatus = require('http-status-codes');
var jsonfile = require('jsonfile');

var FormValidation = require('../shared/form-validation/form-validation.js');
var Response = require('../shared/response.js');
var ModelHelper = require('../shared/model-helper.js');

module.exports = {

    create: function(req, res, next) {
        var formValidation = FormValidation();

        formValidation.setRule('username', [ { validation: 'required' } ]);
        formValidation.setRule('password', [ { validation: 'required' } ]);

        if (!formValidation.run(req.body)) {
            res.status(HttpStatus.BAD_REQUEST)
                .json(Response.api(HttpStatus.BAD_REQUEST, formValidation.errorMessage()));
            return ;
        }

        next();
    },

    userLogin: function(req, res, next) {
        var formValidation = FormValidation();

        formValidation.setRule('username',    [ { validation: 'required' } ]);
        formValidation.setRule('password',    [ { validation: 'required' } ]);
        formValidation.setRule('deviceToken', [ ]);
        formValidation.setRule('deviceType',  [ { validation: 'allowOnly', allows: [ 'iOS', 'Android', 'Web' ] } ]);

        if (!formValidation.run(req.body)) {
            res.status(HttpStatus.BAD_REQUEST)
                .json(Response.api(HttpStatus.BAD_REQUEST, formValidation.errorMessage()));
            return ;
        }

        next();
    },

    deviceRegToken: function(req, res, next){
        var formValidation = FormValidation();

        formValidation.setRule('deviceToken', [ { validation: 'required' } ]);
        formValidation.setRule('deviceType',  [
            { validation: 'required' },
            { validation: 'allowOnly', allows: [ 'iOS', 'Android', 'Web' ] } ]);

        if (!formValidation.run(req.body)) {
            res.status(HttpStatus.BAD_REQUEST)
                .json(Response.api(HttpStatus.BAD_REQUEST, formValidation.errorMessage()));
            return ;
        }

        next();
    },

    addRecipe: function(req, res, next) {

        var formValidation = FormValidation();

        formValidation.setRule('recipe',    [ { validation: 'required' } ]);
        formValidation.setRule('thumbnail', [ { validation: 'required' } ]);
        formValidation.setRule('images',    [ { validation: 'required', allowEmptyArray: true } ]);

        if (!formValidation.run(req.body)) {
            res.status(HttpStatus.BAD_REQUEST)
                .json(Response.api(HttpStatus.BAD_REQUEST, formValidation.errorMessage()));
            return ;
        }

        // --- Parameters are all here now ---
        if (!ModelHelper.isValidBase64Format(req.body.thumbnail)) {
            res.status(HttpStatus.BAD_REQUEST)
                .json(Response.api(HttpStatus.BAD_REQUEST,
                    { message: 'bad_param:thumbnail', detail: 'thumbnail is not base64 encoded image' }));
            return ;
        }

        req.body.images = Array.isArray(req.body.images) ? req.body.images : [req.body.images];
        for (var i = 0; i < req.body.images.length; i++) {
            if (!ModelHelper.isValidBase64Format(req.body.images[i])) {
                res.status(HttpStatus.BAD_REQUEST)
                    .json(Response.api(HttpStatus.BAD_REQUEST,
                        { message: 'bad_param:images', detail: 'images item is not base64 encoded image' }));
                return ;
            }
        }

        ModelHelper.isValidRecipe(req.body.recipe, false)
            .then(function() {
                res.locals.recipe = req.body.recipe;
                next();
            })
            .catch(function(error) {
                res.status(error.status)
                    .json(Response.api(error.status, error.message));
            });
    },

    addRecipeJSON: function(req, res, next) {
        ModelHelper.isValidRecipe(req.body.recipe, true)
            .then(function() {
                res.locals.recipe = req.body.recipe;
                next();
            })
            .catch(function(error) {
                res.status(error.status)
                    .json(Response.api(error.status, error.message));
            });
    },

    editRecipe: function(req, res, next) {

        var formValidation = FormValidation();

        formValidation.setRule('recipe',    [ { validation: 'required' } ]);
        formValidation.setRule('thumbnail', [ { validation: 'required' } ]);
        formValidation.setRule('images',    [ { validation: 'required', allowEmptyArray: true } ]);

        if (!formValidation.run(req.body)) {
            res.status(HttpStatus.BAD_REQUEST)
                .json(Response.api(HttpStatus.BAD_REQUEST, formValidation.errorMessage()));
            return ;
        }

        // --- Parameters are all here now ---
        if (!ModelHelper.isValidBase64Format(req.body.thumbnail)
            && !ModelHelper.isValidUrlFormat(req.body.thumbnail)) {
            res.status(HttpStatus.BAD_REQUEST)
                .json(Response.api(HttpStatus.BAD_REQUEST,
                    { message: 'bad_param:thumbnail', detail: 'thumbnail is not base64 encoded image or a link' }));
            return ;
        }

        req.body.images = Array.isArray(req.body.images) ? req.body.images : [req.body.images];
        for (var i = 0; i < req.body.images.length; i++) {
            if (!ModelHelper.isValidBase64Format(req.body.images[i])
                && !ModelHelper.isValidUrlFormat(req.body.images[i])) {
                res.status(HttpStatus.BAD_REQUEST)
                    .json(Response.api(HttpStatus.BAD_REQUEST,
                        { message: 'bad_param:images', detail: 'images item is not base64 encoded image or a link' }));
                return ;
            }
        }

        ModelHelper.isValidRecipe(req.body.recipe, false)
            .then(function() {
                res.locals.recipe = req.body.recipe;
                next();
            })
            .catch(function(error) {
                res.status(error.status)
                    .json(Response.api(error.status, error.message));
            });
    },

    updateShoppingList: function(req, res, next) {

        if (!req.body.shoppingList) {
            res.status(HttpStatus.BAD_REQUEST)
                .json(Response.api(HttpStatus.BAD_REQUEST, 'field_not_found:shoppingList'));
            return ;
        }

        ModelHelper.isValidShoppingList(req.body.shoppingList)
            .then(function() {
                // req.body.recipe MUST be a valid recipe
                res.locals.shoppingList = req.body.shoppingList;
                next();
            })
            .catch(function(error) {
                res.status(error.status)
                    .json(Response.api(error.status, error.message));
            });
    },

    activateRecipe: function(req, res, next) {
        if (!ModelHelper.isValidObjectId(req.params.id)) {
            res.status(HttpStatus.BAD_REQUEST)
                .json(Response.api(HttpStatus.BAD_REQUEST, 'bad_param:objectid'));
            return ;
        }
        next();
    },

    suspendRecipe: function(req, res, next) {
        if (!ModelHelper.isValidObjectId(req.params.id)) {
            res.status(HttpStatus.BAD_REQUEST)
                .json(Response.api(HttpStatus.BAD_REQUEST, 'bad_param:objectid'));
            return ;
        }
        next();
    },

    deleteRecipe: function(req, res, next) {
        if (!ModelHelper.isValidObjectId(req.params.id)) {
            res.status(HttpStatus.BAD_REQUEST)
                .json(Response.api(HttpStatus.BAD_REQUEST, 'bad_param:objectid'));
            return ;
        }
        next();
    },

    getRecipeById: function(req, res, next) {
        if (!ModelHelper.isValidObjectId(req.params.id)) {
            res.status(HttpStatus.BAD_REQUEST)
                .json(Response.api(HttpStatus.BAD_REQUEST, 'bad_param:objectid'));
            return ;
        }
        next();
    },

    statGetCount: function(req, res, next) {
        var allowedTypes = ['device', 'recipe'];

        if (allowedTypes.indexOf(req.params.type) < 0) {
            res.status(HttpStatus.BAD_REQUEST)
                .json(Response.api(HttpStatus.BAD_REQUEST, 'bad_param:type'));
            return ;
        }

        next();
    }
};
