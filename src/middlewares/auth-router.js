
var express = require('express');
var router = express.Router();

var APIAuth = require('./api-auth.js');

router.post('/user/create', APIAuth.noAuth);
router.post('/user/login', APIAuth.noAuth);

router.post('/device/regToken', APIAuth.noAuth);
router.get('/device', APIAuth.adminLoginRequired);

router.get('/recipe', APIAuth.noAuth);
router.post('/recipe', APIAuth.adminLoginRequired);
router.post('/recipe/full-json', APIAuth.adminLoginRequired);
router.put('/recipe/:id', APIAuth.adminLoginRequired);
router.put('/recipe/:id/activate', APIAuth.adminLoginRequired);
router.put('/recipe/:id/suspend', APIAuth.adminLoginRequired);
router.delete('/recipe/:id', APIAuth.adminLoginRequired);

router.get('/stat/count/:type', APIAuth.adminLoginRequired);

module.exports = router;