
var UserConstants = require('../constants/user.js');
var Config = require('../config/config.js');
var Token = require('../shared/token.js');

module.exports = function(req, res, next) {
    if (req.headers['x-access-token'] === undefined) {
        next(); // nothing need to do if token is absent
    }
    else {
        // maybe there is another kind of login using x-access-token later

        if (res.locals.user) {
            var duration = res.locals.user.role === UserConstants.UserRole.Admin ?
                    Config.accessTokenAdminDuration :
                    Config.accessTokenNormalDuration,
                payload = { user: {
                    _id: res.locals.user._id,
                    username: res.locals.user.username,
                    role: res.locals.user.role } },
                accessToken = Token.generateToken(payload, duration);

            res.locals.responseData.user = {
                _id: res.locals.user._id,
                username: res.locals.user.username,
                role: res.locals.user.role,
                accessToken: accessToken,
                expireAt: new Date((new Date()).setHours((new Date()).getHours() + duration))
            };
        }

        next();
    }
};