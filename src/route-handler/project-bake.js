
var HttpStatus = require('http-status-codes');

var Response = require('../shared/response');
var RecipeConstants = require('../constants/recipe.js');
var storage = require('../shared/storage/storage.js')();

module.exports = function (connection) {

    var RecipeDAO = require('../dao/recipe.js')(connection);
    var ShoppingListDAO = require('../dao/shopping-list.js')(connection);

    return {

        getRecipes: function(req, res) {
            var lastModified = req.headers['if-modified-since'] ? req.headers['if-modified-since'] : null;

            RecipeDAO.getAllRecipes(lastModified)
                .then(function(recipes) {
                    if (recipes.length <= 0) {
                        return Promise.reject(Response.promiseError(HttpStatus.NOT_MODIFIED, 'not_modified'));
                    }
                    res.locals.responseData.recipes = recipes ;
                    return RecipeDAO.getLatestRecipe();
                })
                .then(function(recipe) {
                    res.header('last-modified', new Date(recipe.updatedAt).toUTCString())
                        .header('x-count', res.locals.responseData.recipes.length)
                        .status(HttpStatus.OK)
                        .json(Response.api(HttpStatus.OK, 'ok', res.locals.responseData));
                })
                .catch(function(error) {
                    res.status(error.status)
                        .json(Response.api(error.status, error.message));
                });
        },

        getRecipeById: function(req, res) {
            RecipeDAO.getRecipeById(req.params.id)
                .then(function(recipe) {
                    res.locals.responseData.recipe = recipe;
                    res.json(Response.api(HttpStatus.OK, 'ok', res.locals.responseData));
                })
                .catch(function(error) {
                    res.status(error.status)
                        .json(Response.api(error.status, error.message));
                })
        },

        addRecipe: function(req, res) {
            RecipeDAO.addRecipe(res.locals.recipe)
                .then(function(recipeInserted) {
                    res.locals.responseData._id = recipeInserted._id;
                    res.status(HttpStatus.CREATED)
                        .json(Response.api(HttpStatus.CREATED, 'created', res.locals.responseData));
                })
                .catch(function(error) {
                    res.status(error.status)
                        .json(Response.api(error.status, error.message));
                });
        },

        addRecipeJSON: function(req, res) {
            RecipeDAO.addRecipe(res.locals.recipe)
                .then(function(recipeInserted) {
                    res.locals.responseData._id = recipeInserted._id;
                    res.status(HttpStatus.CREATED)
                        .json(Response.api(HttpStatus.CREATED, 'created', res.locals.responseData));
                })
                .catch(function(error) {
                    res.status(error.status)
                        .json(Response.api(error.status, error.message));
                });
        },

        editRecipe: function(req, res) {
            RecipeDAO.editRecipe(req.params.id, res.locals.recipe)
                .then(function() {
                    res.locals.responseData._id = req.params.id;
                    res.status(HttpStatus.OK)
                        .json(Response.api(HttpStatus.OK, 'ok', res.locals.responseData));
                })
                .catch(function(error) {
                    res.status(error.status)
                        .json(Response.api(error.status, error.message));
                });
        },

        getShoppingList: function(req, res) {
            ShoppingListDAO.getByDeviceId(res.locals.device._id)
                .then(function(ingredients) {
                    res.locals.responseData.ingredients = ingredients;
                    res.status(HttpStatus.OK)
                        .json(Response.api(HttpStatus.OK, 'ok', res.locals.responseData));
                })
                .catch(function(error) {
                    res.status(error.status)
                        .json(Response.api(error.status, error.message));
                });
        },

        updateShoppingList: function(req, res) {
            ShoppingListDAO.updateByDeviceId(res.locals.device._id, req.body.ingredients)
                .then(function() {

                })
                .catch(function() {

                });
        },

        activateRecipe: function(req, res) {
            RecipeDAO.getRecipeById(req.params.id)
                .then(function(recipe) {
                    if (recipe.status === RecipeConstants.RecipeStatus.ACTIVE) {
                        return Promise.reject(Response.promiseError(HttpStatus.NOT_MODIFIED, 'not_modified'));
                    }
                    if (recipe.status === RecipeConstants.RecipeStatus.DELETED) {
                        return Promise.reject(Response.promiseError(HttpStatus.FORBIDDEN, 'forbidden'));
                    }
                    return RecipeDAO.updateRecipeStatus(req.params.id, RecipeConstants.RecipeStatus.ACTIVE);
                })
                .then(function() {
                    res.json(Response.api(HttpStatus.OK, 'ok', res.locals.responseData));
                })
                .catch(function(error) {
                    res.status(error.status).json(Response.api(error.status, error.message));
                });
        },

        suspendRecipe: function(req, res) {
            RecipeDAO.getRecipeById(req.params.id)
                .then(function(recipe) {
                    if (recipe.status === RecipeConstants.RecipeStatus.SUSPENDED) {
                        return Promise.reject(Response.promiseError(HttpStatus.NOT_MODIFIED, 'not_modified'));
                    }
                    if (recipe.status === RecipeConstants.RecipeStatus.DELETED) {
                        return Promise.reject(Response.promiseError(HttpStatus.FORBIDDEN, 'forbidden'));
                    }
                    return RecipeDAO.updateRecipeStatus(req.params.id, RecipeConstants.RecipeStatus.SUSPENDED);
                })
                .then(function() {
                    res.json(Response.api(HttpStatus.OK, 'ok', res.locals.responseData));
                })
                .catch(function(error) {
                    res.status(error.status).json(Response.api(error.status, error.message));
                });
        },

        deleteRecipe: function(req, res) {
            RecipeDAO.getRecipeById(req.params.id)
                .then(function(recipe) {
                    if (recipe.status === RecipeConstants.RecipeStatus.DELETED) {
                        return Promise.reject(Response.promiseError(HttpStatus.FORBIDDEN, 'forbidden'));
                    }
                    return RecipeDAO.updateRecipeStatus(req.params.id, RecipeConstants.RecipeStatus.DELETED);
                })
                .then(function() {
                    res.json(Response.api(HttpStatus.OK, 'ok', res.locals.responseData));
                })
                .catch(function(error) {
                    res.status(error.status).json(Response.api(error.status, error.message));
                });
        }
    }
};