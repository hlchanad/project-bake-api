
var HttpStatus = require('http-status-codes');

var Response = require('../shared/response.js');

module.exports = function(connection) {

    var DeviceDAO = require('../dao/device.js')(connection);

    return {
        regToken: function(req, res){
            DeviceDAO.regToken(req.body.deviceType, req.body.deviceToken)
                .then(function() {
                    res.status(HttpStatus.CREATED)
                        .json(Response.api(HttpStatus.CREATED, 'created', res.locals.responseData));
                })
                .catch(function(error) {
                    res.status(error.status).json(Response.api(error.status, error.message));
                });
        },

        getDevices: function(req, res) {
            DeviceDAO.getDevices()
                .then(function(devices) {
                    res.locals.responseData.devices = devices;
                    res.header('x-count', res.locals.responseData.devices.length)
                        .json(Response.api(HttpStatus.OK, 'ok', res.locals.responseData));
                })
                .catch(function(error) {
                    res.status(error.status).json(Response.api(error.status, error.message));
                });
        }
    }
}