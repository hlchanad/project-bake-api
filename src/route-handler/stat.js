
var HttpStatus = require('http-status-codes');

var Response = require('../shared/response.js');

module.exports = function(connection) {

    var DeviceDAO = require('../dao/device.js')(connection);
    var RecipeDAO = require('../dao/recipe.js')(connection);

    return {
        getCount: function(req, res) {

            var map = {
                recipe: RecipeDAO.countRecipes,
                device: DeviceDAO.countDevices
            };

            map[req.params.type]()
                .then(function(count) {
                    res.json(Response.api(HttpStatus.OK, 'ok', { count: count }));
                })
                .catch(function(error) {
                    res.status(error.status)
                        .json(Response.api(error.status, error.message));
                });
        }
    }
}