
var mongojs = require('mongojs');
var Promise = require('promise');
var HttpStatus = require('http-status-codes');

var Response = require('../shared/response.js');

module.exports = function(connection) {

    var db = mongojs(connection, ['device']);

    return {
        regToken: function(type, token) {
            var _this = this;
            return new Promise(function(resolve, reject) {
                _this.getDeviceByTypeAndToken(type, token)
                    .then(function(device) {
                        resolve();
                    })
                    .catch(function(error) {
                        if (error.status === HttpStatus.SERVICE_UNAVAILABLE) {
                            return Promise.reject(error);
                        }
                        // not found device
                        return _this.createDevice(type, token);
                    })
                    .then(function(device) {
                        resolve();
                    })
                    .catch(function(error) {
                        reject(error);
                    });
            });
        },

        getDevices: function() {
            return new Promise(function(resolve, reject) {
                db.device.find({ token: { $ne: "faked_uuid" } }, function(err, devices) {
                    if (err) {
                        reject(Response.promiseError(HttpStatus.SERVICE_UNAVAILABLE, 'service_unavailable'));
                    }
                    else {
                        resolve(devices);
                    }
                });
            });
        },

        countDevices: function() {
            return new Promise(function(resolve, reject) {
                db.device.count({ token: { $ne: "faked_uuid" } }, function(err, count) {
                    if (err) {
                        reject(Response.promiseError(HttpStatus.SERVICE_UNAVAILABLE, 'service_unavailable'));
                    }
                    else {
                        resolve(count);
                    }
                });
            });
        },

        createDevice: function(type, token) {
            return new Promise(function(resolve, reject) {
                db.device.insert({ type: type, token: token }, function(err, deviceInserted) {
                    if (err) {
                        reject(Response.promiseError(HttpStatus.SERVICE_UNAVAILABLE, 'service_unavailable'));
                    }
                    else {
                        resolve(deviceInserted);
                    }
                });
            });
        },

        getDeviceByTypeAndToken: function(type, token) {
            return new Promise(function(resolve, reject) {
                db.device.findOne({ type: type, token: token }, function(err, device) {
                    if (err) {
                        reject(Response.promiseError(HttpStatus.SERVICE_UNAVAILABLE, 'service_unavailable'));
                    }
                    else if (!device) {
                        reject(Response.promiseError(HttpStatus.NOT_FOUND, 'not_found'));
                    }
                    else {
                        resolve(device);
                    }
                });
            });
        }
    }
}