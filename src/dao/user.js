
var Promise = require('promise');
var mongojs = require('mongojs');
var HttpStatus = require('http-status-codes');
var md5 = require('md5');

var Response = require('../shared/response.js');
var UserConstants = require('../constants/user.js');
var Encrypt = require('../shared/encrypt.js');

/*
user sample:
user : {
  _id: ObjectId,
  username: string,
  password: string,
  device: {
    token: string,
    type: string   // ios/ android
  }
}
 */

module.exports = function(connection) {

    var db = mongojs(connection, ['user']);

    return {
        getUserByUsernameAndPassword: function(username, password) {
            var promise = new Promise(function(resolve, reject) {
                db.user.findOne({ username: username, password: Encrypt.encode(password) }, function(err, user) {
                   if (err) {
                       reject(Response.promiseError(HttpStatus.SERVICE_UNAVAILABLE, 'service_unavailable'));
                   }
                   else if (!user) {
                       reject(Response.promiseError(HttpStatus.UNAUTHORIZED, 'invalid_credential'));
                   }
                   else {
                       resolve(user);
                   }
                });
            });
            return promise;
        },

        isUsernameUnused: function(username) {
            var promise = new Promise(function(resolve, reject) {
                db.user.findOne({ username: username }, function(err, user) {
                    if (err) {
                        reject(Response.promiseError(HttpStatus.SERVICE_UNAVAILABLE, 'service_unavailable'));
                    }
                    else if (user) {
                        reject(Response.promiseError(HttpStatus.UNAUTHORIZED, 'already_used'));
                    }
                    else {
                        resolve();
                    }
                });
            });
            return promise;
        },

        createUser: function(username, password) {
            var promise = new Promise(function(resolve, reject) {
                var user = {
                    username: username,
                    password: md5(password),
                    role: UserConstants.UserRole.Normal
                };

                db.user.insert(user, function(err) {
                    if (err) {
                        reject(Response.promiseError(HttpStatus.SERVICE_UNAVAILABLE, 'service_unavailable'));
                    }
                    else {
                        resolve();
                    }
                });
            });
            return promise;
        },

        updateUserDeviceTokenAndType: function(user, deviceToken, deviceType) {
            var promise = new Promise(function(resolve, reject) {
                db.user.update(
                    { _id: mongojs.ObjectId(user._id) },
                    { $set: { device: { token: deviceToken, type: deviceType} } },
                    function(err) {
                        if (err) {
                            reject(Response.promiseError(HttpStatus.SERVICE_UNAVAILABLE, 'service_unavailable'));
                        }
                        else {
                            resolve();
                        }
                    }
                )
            });
            return promise;
        }

    }
}