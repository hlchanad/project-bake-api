
var mongojs = require('mongojs');
var Promise = require('promise');
var HttpStatus = require('http-status-codes');

var Response = require('../shared/response.js');
var RecipeConstants = require('../constants/recipe.js');

module.exports = function(connection) {

    var db = mongojs(connection, ['recipe']);

    return {
        getAllRecipes: function(lastModified) {
            var query = lastModified ? { "updatedAt": { "$gte": new Date(lastModified) } } : {};

            return new Promise(function(resolve, reject) {
                db.recipe.find(query, function (err, recipes) {
                    if (err) {
                        reject(Response.promiseError(HttpStatus.SERVICE_UNAVAILABLE, 'service_unavailable'));
                    }
                    else {
                        resolve(recipes);
                    }
                });
            });
        },

        countRecipes: function() {
            return new Promise(function(resolve, reject) {
                db.recipe.count({ status: { $ne: RecipeConstants.RecipeStatus.DELETED } }, function(err, count) {
                    if (err) {
                        reject(Response.promiseError(HttpStatus.SERVICE_UNAVAILABLE, 'service_unavailable'));
                    }
                    else {
                        resolve(count);
                    }
                });
            });
        },

        getRecipeById: function(recipeId) {
            return new Promise(function(resolve, reject) {
                db.recipe.findOne({ _id: mongojs.ObjectId(recipeId) }, function(err, recipe) {
                    if (err) {
                        reject(Response.promiseError(HttpStatus.SERVICE_UNAVAILABLE, 'service_unavailable'));
                    }
                    else if (!recipe) {
                        reject(Response.promiseError(HttpStatus.NOT_FOUND, 'not_found'));
                    }
                    else {
                        resolve(recipe);
                    }
                });
            });
        },

        getLatestRecipe: function() {
            return new Promise(function(resolve, reject) {
                db.recipe.find().sort({ updatedAt: -1}).limit(1, function(err, recipes) {

                    if (err) {
                        reject(Response.promiseError(HttpStatus.SERVICE_UNAVAILABLE, 'service_unavailable'));
                    }
                    else {
                        resolve(recipes[0]);
                    }
                });
            })
        },

        addRecipe: function(recipe) {

            // some preset values
            recipe.createdAt = new Date();
            recipe.updatedAt = new Date();
            recipe.status = RecipeConstants.RecipeStatus.ACTIVE;

            return new Promise(function(resolve, reject) {
                db.recipe.insert(recipe, function (err, recipeInserted) {
                    if (err) {
                        reject(Response.promiseError(HttpStatus.SERVICE_UNAVAILABLE, 'service_unavailable'));
                    }
                    else {
                        resolve(recipeInserted);
                    }
                });
            });
        },

        editRecipe: function(recipeId, recipe) {

            // some preset values
            recipe.updatedAt = new Date();

            return new Promise(function(resolve, reject) {
                db.recipe.update(
                    { _id: mongojs.ObjectId(recipeId) },
                    { $set: {
                        title: recipe.title,
                        desc: recipe.desc,
                        size: recipe.size,
                        parts: recipe.parts,
                        title_zh: recipe.title_zh,
                        desc_zh: recipe.desc_zh,
                        size_zh: recipe.size_zh,
                        parts_zh: recipe.parts_zh,
                        images: recipe.images,
                        thumbnail: recipe.thumbnail,
                        updatedAt: recipe.updatedAt
                    } },
                    function(err) {
                        if (err) {
                            reject(Response.promiseError(HttpStatus.SERVICE_UNAVAILABLE, 'service_unavailable'));
                        }
                        else {
                            resolve();
                        }
                    });
            });
        },

        updateRecipeStatus: function(recipeId, status) {
            return new Promise(function(resolve, reject) {
                db.recipe.update(
                    { _id: mongojs.ObjectId(recipeId) },
                    { $set: { status: status, updatedAt: new Date() } },
                    function(err) {
                        if (err) {
                            reject(Response.promiseError(HttpStatus.SERVICE_UNAVAILABLE, 'service_unavailable'));
                        }
                        else {
                            resolve();
                        }
                    });
            });
        }
    }
}