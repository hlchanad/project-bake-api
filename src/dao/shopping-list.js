
var mongojs = require('mongojs');
var Promise = require('promise');
var HttpStatus = require('http-status-codes');

var Response = require('../shared/response.js');

module.exports = function(connection) {

    var db = mongojs(connection, ['shopping_list']);

    return {

        getByDeviceId: function(deviceId) {
            var promise = new Promise(function(resolve, reject){
                db.shopping_list.find({ deviceId: mongojs.ObjectId(deviceId) }, function(err, ingredients) {
                    if (err) {
                        reject(Response.promiseError(HttpStatus.SERVICE_UNAVAILABLE, 'service_unavailable'));
                    }
                    else {
                        resolve(ingredients);
                    }
                });
            });
            return promise;
        },

        updateByDeviceId: function(deviceId, ingredients) {
            var promise = new Promise(function(resolve, reject) {
                db.shopping_list.update(
                    { deviceId: mongojs.ObjectId(deviceId)},
                    { $set: { ingredients: ingredients } },
                    function(err) {
                        if (err) {
                            reject(Response.promiseError(HttpStatus.SERVICE_UNAVAILABLE, 'service_unavailable'));
                        }
                        else {
                            resolve();
                        }
                    }
                )
            });
            return promise;
        },

        removeByDeviceIdandRecipeId: function(deviceId, recipeId) {

        }
    }
}