
module.exports = {
    RecipeStatus: {
        ACTIVE: 1,
        SUSPENDED: 2,
        DELETED: 0
    }
}