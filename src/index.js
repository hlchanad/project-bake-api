
var Config = require('./config/config');

var express = require('express');
var router = express.Router();

var connection = Config.database.connectionString;
var projectBake = require('./route-handler/project-bake.js')(connection);
var user = require('./route-handler/user.js')(connection);
var device = require('./route-handler/device.js')(connection);
var stat = require('./route-handler/stat.js')(connection);

var authRouter = require('./middlewares/auth-router.js');
var formValidationRouter = require('./middlewares/form-validation-router.js');
var fileUploadRouter = require('./middlewares/file-upload-router.js');
var updateAccessToken = require('./middlewares/update-access-token.js');

// init responseData for later use, will be returned at the end
router.use(function(req, res, next) { res.locals.responseData = {}; next(); });

// handle access token things
router.use(authRouter);

// handle form validations in middleware before doing real things
router.use(formValidationRouter);

// handle any file upload tasks
router.use(fileUploadRouter);

// update token if present
router.use(updateAccessToken);

// --- REAL PATHS ---

router.post('/user/create', user.create);
router.post('/user/login', user.login);

router.post('/device/regToken', device.regToken);
router.get('/device', device.getDevices);

router.get('/recipe', projectBake.getRecipes);
router.get('/recipe/:id', projectBake.getRecipeById);
router.post('/recipe', projectBake.addRecipe);
router.post('/recipe/full-json', projectBake.addRecipeJSON);
router.put('/recipe/:id', projectBake.editRecipe);
router.put('/recipe/:id/activate', projectBake.activateRecipe);
router.put('/recipe/:id/suspend', projectBake.suspendRecipe);
router.delete('/recipe/:id', projectBake.deleteRecipe);

router.get('/stat/count/:type', stat.getCount);

router.get('/health-check', (req, res) => res.send('ok'))

module.exports = router;
