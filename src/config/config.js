
module.exports = {
    appName: 'Project Bake',
    jwtSecret: 'this is a secret key!',
    accessTokenNormalDuration: 24*14, // unit: hours
    accessTokenAdminDuration: 2, // unit: hours
    database: {
        connectionString: process.env.DATABASE_CONNECTION_STRING,
    },
    server: {
        port: process.env.PORT || 4000,
    },
}