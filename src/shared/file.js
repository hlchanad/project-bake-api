
var jsonfile = require('jsonfile');

module.exports = {

    readJsonSchema: function (schemaName) {

        // don't have idea why i am at root index.js lol, maybe because this file is loaded by root index.js eventually
        var jsonPath = 'routes/project-bake/json-schemas/';

        return new Promise(function (resolve, reject) {
            jsonfile.readFile(jsonPath + schemaName + '.json', function (err, schemaObj) {
                if (err) {
                    reject(err);
                }
                else {
                    resolve(schemaObj);
                }
            });
        });
    }
}