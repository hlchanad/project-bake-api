
var HttpStatus = require('http-status-codes');
var promise = require('promise');
var Ajv = require('ajv');

var Response = require('./response.js');
var File = require('./file.js');

module.exports = {

    isValidRecipe: function(recipe, isFullRecipe) {

        var schema = isFullRecipe ? 'recipe-full' : 'recipe-for-add';

        return new Promise(function(resolve, reject) {
            File.readJsonSchema(schema)
                .then(function(schema) {

                    var ajv = new Ajv();
                    var valid = ajv.validate(schema, recipe);

                    if (!valid) {
                        reject(Response.promiseError(
                            HttpStatus.BAD_REQUEST, { message: 'bad_param:recipe', detail: ajv.errorsText() }));
                    }
                    else {
                        resolve();
                    }
                })
                .catch(function(error) {
                    console.log(error);
                    reject(Response.promiseError(HttpStatus.SERVICE_UNAVAILABLE, 'service_unavailable'));
                });
        });
    },

    isValidShoppingList: function(shoppingList) {
        return new Promise(function(resolve, reject) {
            resolve();
        });
    },

    isValidObjectId: function(objectId) {
        return objectId.match(/^[a-fA-F0-9]{24}$/);
    },

    isValidBase64Format: function(dataString) {
        var matches = dataString.match(/^data:([A-Za-z-+\/]+);base64,(.+)$/);
        return matches && matches.length === 3;
    },

    isValidUrlFormat: function(url) {
        return url.match(/^https?:\/\/.+$/);
    }
}