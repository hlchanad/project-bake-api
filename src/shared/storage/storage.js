
var Util = require('../util.js');

var HttpStatus = require('http-status-codes');
var localStorage = require('./storage-local.js');
var s3Storage = require('./storage-s3.js');

var Response = require('../response.js');
var Config = require('./config.js');

module.exports = function(method) {

    var allowedMethod = Object.values(Config.General.MethodAllowed);

    method = method && allowedMethod.indexOf(method) >= 0 ? method : Config.General.DefaultMethod;

    var instance;
    switch (method) {
        case Config.General.MethodAllowed.S3:    instance = s3Storage();    break;
        case Config.General.MethodAllowed.Local: instance = localStorage(); break;
    }

    return {
        isObjectExist: function(folder, filename, extension) {
            return new Promise(function(resolve, reject) {
                instance.isObjectExist(folder, filename, extension)
                    .then(function(isExist) {
                        resolve(isExist);
                    })
                    .catch(function(error) {
                        reject(Response.promiseError(HttpStatus.SERVICE_UNAVAILABLE, 'service_unavailable'));
                    });
            });
        },

        /**
         * return `{ link: link, payload: payload }` if payload is present
         * return `link` if payload is absent
         *
         * @param dataString
         * @param folder
         * @param filename
         * @param isPrivate
         * @param payload
         */
        storeBase64Image: function(dataString, folder, filename, isPrivate, payload) {
            var _this = this;

            return new Promise(function(resolve, reject) {
                var imageBuffer = Util.decodeBase64Image(dataString);

                filename = filename ? filename : 'upload_' + (new Date()).getTime();
                folder = folder ? folder : Config.General.imageFolder;

                _this.isObjectExist(folder, filename, imageBuffer.extension)
                    .then(function(isExist) {
                        filename += isExist ? '-' + Util.randomString(6) : '';

                        instance.storeBase64Image(imageBuffer.data, folder, filename, imageBuffer.extension, isPrivate)
                            .then(function(link) {
                                if (payload !== undefined) {
                                    resolve({ link: link, payload: payload });
                                }
                                else {
                                    resolve(link);
                                }
                            })
                            .catch(function(error) { reject(error); });
                    })
                    .catch(function(error) {
                        reject(Response.promiseError(HttpStatus.SERVICE_UNAVAILABLE, 'service_unavailable'));
                    });
            });
        }
    }
}