
var Config = require('./config.js');
var AWS = require('aws-sdk');
var s3 = new AWS.S3({ params: { Bucket: Config.S3.bucketName } });

module.exports = function() {
    console.log('storage-s3 constructor');

    return {
        isObjectExist: function(folder, filename, extension) {
            return new Promise(function(resolve, reject) {
                var params = { Key: folder + filename + '.' + extension };

                s3.getObject(params, function(err, data) {
                    if (err) {
                        if (err.code === 'NoSuchKey') { resolve(false); }
                        else { reject(); }
                    }
                    else if (data) { resolve(true); }
                    else { resolve(false); }
                });
            });
        },

        storeBase64Image: function(imageBuffer, folder, filename, extension, isPrivate) {
            return new Promise(function(resolve, reject) {
                var path = folder + filename + '.' + extension;

                var params = {
                    Key: path,         // filename
                    Body: imageBuffer,
                    ContentEncoding: 'base64',
                    ContentType: 'image/' + extension,
                    ACL: !!isPrivate ? 'private' : 'public-read' // default is public-read
                };

                s3.putObject(params, function(err, data){
                    if (err) { reject(err); }
                    else {
                        resolve(isPrivate ? '' : Config.S3.baseUrl + Config.S3.bucketName + '/' + path);
                    }
                });
            });
        }
    }
}