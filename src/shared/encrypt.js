/**
 * Created by chanhonlun on 11/3/2018.
 */

var md5 = require('md5');

module.exports = {

    encode: function(string) {
        return md5(string);
    }

};