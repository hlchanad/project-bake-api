
module.exports = {

    /**
     * return structured api response
     *
     * note: if message is { message: string, detail: string }, one more property 'detail' will be attached to 'result'
     *
     * @param status [Number]
     * @param message [Object|String]
     * @param data [Object]
     * @returns {{result: {status: *, message: *}, data: *}}
     */
    api: function (status, message, data) {

        var response = {
            result: {
                status: status
            }
        };

        if (typeof message === 'object') {
            if (message.message) { response.result.message = message.message; }
            if (message.detail)  { response.result.detail  = message.detail; }
        }
        else {
            response.result.message = message;
        }

        if (data) { response.data = data; }

        return response;
    },

    /**
     * return structured format for promise error
     *
     * @param status
     * @param message
     * @returns {{status: *, message: *}}
     */
    promiseError: function(status, message) {
        return {
            status: status,
            message: message
        }
    }
}