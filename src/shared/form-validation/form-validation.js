
var errorMessage = require('./lang.js');

module.exports = function () {

    var fields = [];
    var error = '';

    function setError(field, rule) {
        error = errorMessage[rule.validation].replace('{field}', field);
    }

    return {
        setRule: function(field, rules) {
            // rule: only support 'required' as of now

            if (!Array.isArray(rules)) {
                rules = [rules];
            }

            fields.push({
                field: field,
                rules: rules
            });
        },

        run: function(data) {

            for (var i=0; i<fields.length; i++) {
                var field = fields[i];

                for (var j=0; j<field.rules.length; j++) {
                    var rule = field.rules[j];

                    switch (rule.validation) {
                        case 'required':
                            if (Array.isArray(data[field.field])
                                && data[field.field].length <= 0
                                && rule.allowEmptyArray ) {
                                break;
                            }

                            if (data[field.field] === undefined
                                || Array.isArray(data[field.field]) && data[field.field].length <= 0
                                || data[field.field].toString() === '') {
                                setError(field.field, rule);
                                return false;
                            }
                            break;
                        case 'allowOnly':
                            if (data[field.field] !== undefined && rule.allows.indexOf(data[field.field]) < 0) {
                                setError(field.field, rule);
                                return false ;
                            }
                            break ;
                    }
                }
            }
            return true;
        },

        errorMessage: function() {
            return error;
        }
    }
}