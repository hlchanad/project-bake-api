
module.exports = {
    decodeBase64Image: function (dataString) {

        var matches = dataString.match(/^data:([A-Za-z-+\/]+);base64,(.+)$/);

        if (matches.length !== 3) {
            return new Error('Invalid input string');
        }

        return {
            type: matches[1],
            extension: matches[1].split('/')[1], // image/jpeg, image/png, image/gif
            data: new Buffer(matches[2], 'base64')
        }
    },

    randomString: function(length, type) {
        var text = "", possible;

        switch(type) {
            case 'number':
                possible = '0123456789';
                break;

            case 'alnum-upper':
                possible = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
                break;

            case 'alnum-full':
                possible = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
                break;

            case 'alnum-lower':
            default:
                possible = 'abcdefghijklmnopqrstuvwxyz0123456789';
                break;
        }

        for (var i = 0; i < length; i++)
            text += possible.charAt(Math.floor(Math.random() * possible.length));

        return text;
    }
}