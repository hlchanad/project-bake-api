FROM node:14.3.0-alpine3.11 as builder

WORKDIR /usr/app

COPY ./package*.json ./
RUN npm install

COPY ./ ./

CMD npm run start
